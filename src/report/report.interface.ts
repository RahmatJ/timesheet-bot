import { TimeSheetStatusEnum } from '../timeSheet/timeSheet.enum';

export interface ITimeSheetDataReport {
  date: string;
  workingHours: string;
  activity: string;
  status: TimeSheetStatusEnum;
}

export interface ITimeSheetReport {
  date: string;
  normalShift: string;
  name: string;
  position: string;
  data: ITimeSheetDataReport[];
}

export interface IReportColumn {
  key: string;
  header: string;
}
