import reportHelper from './report.helper';
import timeSheetService from '../timeSheet/timeSheet.service';
import { IReportColumn } from './report.interface';

const generateTimeSheetReport = async (
  userId: string,
  month: number,
  year: number
) => {
  const timeSheetData = await timeSheetService.findTimeRecords(
    userId,
    month,
    year
  );
  const data = reportHelper.mapTimeSheetReportData(timeSheetData, month, year);

  const column: IReportColumn[] = [
    { key: 'date', header: 'Date' },
    { key: 'workingHours', header: 'Working Hours' },
    { key: 'activity', header: 'Activity' },
    { key: 'status', header: 'Status' }
  ];

  return reportHelper.generateExcelFile(column, data.data);
};

const reportService = { generateTimeSheetReport };
export default reportService;
