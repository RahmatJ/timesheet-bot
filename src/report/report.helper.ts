import { ITimeSheet } from '../timeSheet/timeSheet.interface';
import moment from 'moment-timezone';
import {
  IReportColumn,
  ITimeSheetDataReport,
  ITimeSheetReport
} from './report.interface';
import Excel from 'exceljs';
import * as path from 'path';

const mapTimeSheetReportData = (
  timeSheet: ITimeSheet[],
  // user: IUser, TODO: added later
  month: number,
  year: number
) => {
  /*
  basicData: {
    Date: <Start Date of month w/o year> - <End Date of month with year> TODO: generated based on input
    Normal Shift: string with format {startHour - endHour} e.g: 09.00 - 18.00. from user.workingHour TODO: added later
    Name: from user.userName TODO: added later
    Position: from user.position TODO: added later
  }
  timeSheetData: {
    Date: auto iterate <only working day> format: DD MM e.g 01 Desember
    Working Hours: from user.position TODO: added later
    Activity: from timeSheet.description
    Status: from timeSheet.status
  }
 */

  // TODO: will use data from table user
  const workingHour = '09.00 - 18.00';
  const name = 'Rahmat Jayanto';
  const position = 'Back End Engineer';
  const formattedTimeSheet: ITimeSheetDataReport[] = [];
  const monthDate = moment().set({ month: month - 1, year });
  const startDate = monthDate.startOf('month').format('DD MMMM');
  const endDate = monthDate.endOf('month').format('DD MMMM');

  timeSheet.forEach((sheet) => {
    const date = moment(sheet.date).format('DD MMMM YYYY');
    const dataSheet: ITimeSheetDataReport = {
      activity: sheet.description,
      date,
      status: sheet.status,
      workingHours: workingHour
    };
    formattedTimeSheet.push(dataSheet);
  });
  const workingDate = `${startDate} - ${endDate}`;
  const mappedTimeSheet: ITimeSheetReport = {
    data: formattedTimeSheet,
    date: workingDate,
    name,
    normalShift: workingHour,
    position
  };
  return mappedTimeSheet;
};

const generateExcelFile = async <T>(columns: IReportColumn[], data: T[]) => {
  const workBook = new Excel.Workbook();
  const workSheet = workBook.addWorksheet();
  workSheet.columns = columns;

  data.forEach((item) => workSheet.addRow(item));
  const exportPath = path.resolve(__dirname, '../output/data.xlsx');
  await workBook.xlsx.writeFile(exportPath);
  return exportPath;
};
const reportHelper = { mapTimeSheetReportData, generateExcelFile };
export default reportHelper;
