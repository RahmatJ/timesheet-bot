import Discord, { Events } from 'discord.js';
import { TimeSheetStatusEnum } from '../timeSheet/timeSheet.enum';
import clientHelper from './client.helper';
import { ICommandValue } from '../common/interface';

const responseCommand = async (msg: Discord.Message) => {
  const args = msg.content.substring(1).split(' ');
  const cmd = args[0];

  const userId = msg.author.id;
  const userName = msg.author.username;
  let commandValue: ICommandValue;

  switch (cmd) {
    case 'ping':
      msg.channel.send(`Hello from AI bot ${msg.author.username}`);
      break;
    case 'insertDone':
      await clientHelper.insertTimeSheetData(
        msg,
        userId,
        TimeSheetStatusEnum.DONE,
        args.slice(1)
      );
      break;
    case 'insertPending':
      await clientHelper.insertTimeSheetData(
        msg,
        userId,
        TimeSheetStatusEnum.PENDING,
        args.slice(1)
      );
      break;
    case 'insertOnProgress':
      await clientHelper.insertTimeSheetData(
        msg,
        userId,
        TimeSheetStatusEnum.ON_PROGRESS,
        args.slice(1)
      );
      break;
    case 'find':
      // --month=x;
      // --year=y;
      commandValue = clientHelper.getMonthAndYear(args);
      // const month = 1;
      // const year = 2023;
      await clientHelper.findTimeSheetData(
        msg,
        userId,
        userName,
        commandValue.month,
        commandValue.year
      );
      break;
    case 'generateReport':
      // --month=x
      // --year=y
      commandValue = clientHelper.getMonthAndYear(args);
      // const reportMonth = Number(args[1]);
      // const reportYear = Number(args[2]);
      await clientHelper.generateTimeSheetByYearAndMonth(
        msg,
        userId,
        commandValue.month,
        commandValue.year
      );
      break;
    default:
      msg.channel.send(`Unknown command, please try !help`);
  }
};

const initiateDiscordClient = (): Discord.Client => {
  const intents: number[] = [
    Discord.GatewayIntentBits.Guilds,
    Discord.GatewayIntentBits.GuildMessages,
    Discord.GatewayIntentBits.MessageContent
  ];
  const client = new Discord.Client({ intents });

  client.on(Events.ClientReady, () => {
    if (!client.user) {
      console.log('user not found');
      throw new Error('user not found');
    }
    console.log(`Logged in as ${client.user.tag}! we are online`);
  });

  client.on(Events.MessageCreate, async (msg) => {
    if (msg.content.substring(0, 1) === '!') {
      await responseCommand(msg);
    }
  });

  return client;
};

const discordClient = { initiateDiscordClient };
export default discordClient;
