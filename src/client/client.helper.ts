import Discord from 'discord.js';
import timeSheetService from '../timeSheet/timeSheet.service';
import { TimeSheetStatusEnum } from '../timeSheet/timeSheet.enum';
import reportService from '../report/report.service';
import { ICommandValue } from '../common/interface';

const insertTimeSheetData = async (
  msg: Discord.Message,
  userId: string,
  status: TimeSheetStatusEnum,
  description: string[]
) => {
  await timeSheetService.createTimeRecord(userId, description, status);
  msg.channel.send(`Activity saved!`);
};

const findTimeSheetData = async (
  msg: Discord.Message,
  userId: string,
  userName: string,
  month: number,
  year: number
) => {
  msg.channel.send(
    `userName: ${userName}, month: ${month}, year: ${year}, userId: ${userId}`
  );
  const result = await timeSheetService.findTimeRecords(userId, month, year);
  result.map((data) => {
    msg.channel.send(`result: ${JSON.stringify(data)}`);
  });
};

const generateTimeSheetByYearAndMonth = async (
  msg: Discord.Message,
  userId: string,
  reportMonth: number,
  reportYear: number
) => {
  const path = await reportService.generateTimeSheetReport(
    userId,
    reportMonth,
    reportYear
  );
  await msg.channel.send({
    files: [{ attachment: path, name: 'data.xlsx' }]
  });
  msg.channel.send(`report generated!`);
};

const getMonthAndYear = (args: string[]): ICommandValue => {
  let month = 0;
  let year = 0;
  let description = '';
  const inputData = args.slice(1);
  for (const input of inputData) {
    if (input.includes('=')) {
      const data = input.split('=');
      const command = data[0];
      const value = data[1];
      if (command === '--month') {
        month = Number(value);
        continue;
      }
      if (command === '--year') {
        year = Number(value);
        continue;
      }
    }

    description += `${input} `;
  }
  return { month, year, description: description.trim() };
};

const clientHelper = {
  insertTimeSheetData,
  findTimeSheetData,
  generateTimeSheetByYearAndMonth,
  getMonthAndYear
};
export default clientHelper;
