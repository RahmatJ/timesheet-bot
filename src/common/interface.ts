export interface ICommandValue {
  month: number;
  year: number;
  description: string;
}
