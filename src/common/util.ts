import { Document } from 'mongoose';

const getMongoURI = (
  user: string,
  password: string,
  dbName: string,
  dbHosts: string
) => {
  const baseConnectionPrefix = 'mongodb';
  return `${baseConnectionPrefix}://${user}:${password}@${dbHosts}/${dbName}`;
};

const documentToObject = <T, DB>(document: DB & Document): T =>
  document.toObject({ getters: true });

const util = { getMongoURI, documentToObject };
export default util;
