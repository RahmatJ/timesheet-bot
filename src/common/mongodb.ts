/* eslint-disable @typescript-eslint/no-explicit-any */
import { connect, connection } from 'mongoose';
import util from './util';

export const MONGO_USER_PARAM = 'MONGO_USER';
export const MONGO_DB_NAME = 'MONGO_DB_NAME';
export const MONGO_HOSTS = 'MONGO_HOSTS';

export const connectMongo = () =>
  new Promise<void>((resolve, reject) => {
    const user = process.env.MONGO_USER;
    const pass = process.env.MONGO_PASSWORD || 'secret';
    const dbName = process.env.MONGO_DB_NAME;
    const dbHosts = process.env.MONGO_HOSTS;

    if (!user) {
      console.error(`No ${MONGO_USER_PARAM} is provided`);
      return reject(`No ${MONGO_USER_PARAM} is provided`);
    }
    if (!dbHosts) {
      console.error(`No ${MONGO_HOSTS} is provided`);
      return reject(`No ${MONGO_HOSTS} is provided`);
    }
    if (!dbName) {
      console.error(`No ${MONGO_DB_NAME} is provided`);
      return reject(`No ${MONGO_DB_NAME} is provided`);
    }

    const dbUri = util.getMongoURI(user, pass, dbName, dbHosts);

    connection.on('error', (err: any) => {
      console.error('error while connecting to mongodb', err);
    });

    connection.once('error', reject); // reject first error

    connection.once('open', () => {
      connection.off('error', reject);
      resolve();
    });

    connection.on('reconnected', () => {
      console.log('Connection to mongodb is resumed');
    });

    connection.on('disconnected', () => {
      console.error('Mongodb disconnected');
    });

    connect(dbUri, {
      user,
      pass,
      dbName
    });
  });
