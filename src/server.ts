import dotenv from 'dotenv';
import { connectMongo } from './common/mongodb';
import moment from 'moment-timezone';
import discordClient from './client/client';

dotenv.config();
const main = async () => {
  moment.locale('id');

  const client = discordClient.initiateDiscordClient();

  try {
    await connectMongo();
    console.log('Database connected');
    await client.login(process.env.CLIENT_TOKEN);
  } catch (e) {
    console.error(`Bot cannot start ${e}`);
    process.exit(1);
  }
};

main();
