import {
  ITimeSheet,
  ITimeSheetDB,
  ITimeSheetFindQuery
} from './timeSheet.interface';
import moment from 'moment-timezone';
import timeSheetRepository from './timeSheet.repository';
import timeSheetHelper from './timeSheet.helper';
import { TimeSheetStatusEnum } from './timeSheet.enum';

const createTimeRecord = async (
  userId: string,
  description: string[],
  activityStatus: TimeSheetStatusEnum
) => {
  const payload = timeSheetHelper.mapCreateTimeSheetPayload(
    userId,
    description,
    activityStatus
  );
  // needed: userID, description, status
  const date = moment().toDate();
  // currently static userName, later using user db
  const userName = 'Rahmat Jayanto';
  const insertPayload: ITimeSheet = {
    date,
    userName,
    description: payload.description,
    status: payload.status,
    userId: payload.userId
  };

  return timeSheetRepository.createTimeRecord(insertPayload);
};

const findTimeRecords = async (
  userId: string,
  month: number,
  year: number
): Promise<ITimeSheetDB[]> => {
  const startDate = moment()
    .set({ month: month - 2, year: year })
    .endOf('month')
    .toDate();
  const endDate = moment()
    .set({ month: month - 1, year: year })
    .endOf('month')
    .toDate();

  const query: ITimeSheetFindQuery = { endDate, startDate, userId };

  return await timeSheetRepository.findTimeRecords(query);
};

const timeSheetService = { createTimeRecord, findTimeRecords };
export default timeSheetService;
