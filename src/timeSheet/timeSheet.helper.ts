import { TimeSheetStatusEnum } from './timeSheet.enum';

const mapCreateTimeSheetPayload = (
  userId: string,
  rawDescription: string[],
  status: TimeSheetStatusEnum
) => {
  const description = rawDescription.join(' ');

  return {
    description,
    status,
    userId
  };
};
const timeSheetHelper = { mapCreateTimeSheetPayload };
export default timeSheetHelper;
