import { ITimeSheetDB } from './timeSheet.interface';
import mongoose, { Document, Schema } from 'mongoose';
import { TimeSheetStatusEnum } from './timeSheet.enum';

export type timeSheetDocument = ITimeSheetDB & Document;
export const timeSheetCollectionName = 'time_sheets';
const timeSheetSchemaDefinition = {
  userName: {
    required: true,
    type: String
  },
  userId: {
    required: true,
    type: String
  },
  date: {
    required: true,
    type: Date,
    default: new Date()
  }, // yy - mm - dd
  description: {
    required: true,
    type: String
  },
  status: {
    required: true,
    type: String,
    enum: TimeSheetStatusEnum
  }
};

const timeSheetSchema = new Schema(timeSheetSchemaDefinition, {
  collection: timeSheetCollectionName,
  timestamps: true
});

timeSheetSchema.index({ userId: 1, date: 1, status: 1 });

export const timeSheetModel = mongoose.model<timeSheetDocument>(
  timeSheetCollectionName,
  timeSheetSchema
);
