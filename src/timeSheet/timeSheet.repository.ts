import { timeSheetDocument, timeSheetModel } from './timeSheet.model';
import {
  ITimeSheet,
  ITimeSheetDB,
  ITimeSheetFindQuery
} from './timeSheet.interface';

const documentToObject = (document: timeSheetDocument): ITimeSheetDB =>
  document.toObject({ getters: true });

const createTimeRecord = async (payload: ITimeSheet) => {
  const result: timeSheetDocument = await timeSheetModel.create(payload);
  return documentToObject(result);
};

const findTimeRecords = async (query: ITimeSheetFindQuery) => {
  const queryData = {
    userId: query.userId,
    date: { $gt: query.startDate, $lte: query.endDate }
  };
  const result: timeSheetDocument[] = await timeSheetModel
    .find(queryData)
    .sort({ createdAt: 1 })
    .exec();
  return result.map((data) => documentToObject(data));
};

const timeSheetRepository = {
  createTimeRecord,
  findTimeRecords
};
export default timeSheetRepository;
