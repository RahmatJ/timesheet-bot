export enum TimeSheetStatusEnum {
  PENDING = 'PENDING',
  ON_PROGRESS = 'ON_PROGRESS',
  DONE = 'DONE'
}
