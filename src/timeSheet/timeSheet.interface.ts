import { ObjectId } from 'mongodb';
import { TimeSheetStatusEnum } from './timeSheet.enum';

export interface ITimeSheet {
  userName: string;
  userId: string;
  date: Date; // yy - mm - dd
  description: string;
  status: TimeSheetStatusEnum;
}

export interface ITimeSheetDB extends ITimeSheet {
  _id: ObjectId;
  createdAt: Date;
  updatedAt: Date;
}

export interface ITimeSheetFindQuery {
  userId: string;
  startDate: Date;
  endDate: Date;
}

export interface ITimeSheetPayload
  extends Pick<ITimeSheet, 'description' | 'status'> {
  userId: string;
}
