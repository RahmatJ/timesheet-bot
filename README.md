# My Timesheet bot



## Description

My Time Sheet bot

## .env
To run this code properly you need `.env` file in the root directory. 
This is the content you need to put in the file
```
CLIENT_TOKEN= [put your discord bot token here]
MONGO_USER= [put your mongo username here, better if it got read and write access]
MONGO_DB_NAME= [put your mongo database here]
MONGO_HOSTS= [put your mongodb host here, format: <IP>:<PORT>]
MONGO_PASSWORD= [put your mongodb password here]
```

## Step to run

- clone this repo
- run `npm install`
- run `npm start`

## Any question or suggestion
- Don't hesitate to get in touch with me, or just made issue or merge request
